<?php

$params = require(__DIR__ . '/params.php');

$db = require(__DIR__ . '/db.php');

return [
    'id' => 'rest-api',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'rest\versions\v1\RestModule',
        ],
        'gii' => [
            'class' => 'yii\gii\Module'
        ],
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => 'null',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/type', 'v1/status'],
                    'only' => ['index', 'options']
                ],
                ['class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/project',
                    'tokens' => [
                        '{id}' => '<id:\w+>'
                    ],
                ],
                ['class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/task'],
                    'tokens' => [
                        //'{id}' => '<id:\w+-\d*>',
                        '{id}' => '<id:\d*>',
                        '{pid}' => '<project:\w+>',
                        '{upd}' => '<id:\d+,\w+>'
                    ],
                    'patterns' => [
                        'GET,HEAD {id}' => 'view',
                        'GET,HEAD {pid}' => 'project',
                        'GET,HEAD {pid}-{id}' => 'project',
                        'PUT,PATCH {upd}' => 'update',
                        'DELETE {id}' => 'delete',
                        'POST' => 'create',
                        'GET,HEAD' => 'index',
                        '{upd}' => 'options',
                        '{pid}' => 'options',
                        '{pid}-{id}' => 'options',
                        '{id}' => 'options',
                        '' => 'options',
                    ],
                ],
            ],
        ],
        'db' => $db
    ],
    'params' => $params,
];
