<?php

namespace rest\versions\v1\modules;

use Yii;
use yii\rest\ViewAction;
use rest\versions\v1\models\Tasks;
use \yii\data\ActiveDataProvider;

class ProjectAction extends ViewAction {

  public function run($project, $id = false) {
    if($id) {
      $model = $this->findModel($id . ',' . $project);
      if ($this->checkAccess) {
        call_user_func($this->checkAccess, $this->id, $model);
      }
      return $model;
    } else {
      $model = new $this->modelClass;
      $provider = new ActiveDataProvider([
        'query' => Tasks::find()->where(['project_id' => $project]),
      ]);
      if ($this->checkAccess) {
        call_user_func($this->checkAccess, $this->id, $model);
      }
      return $provider;
    }
  }
}
