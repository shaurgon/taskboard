<?php

namespace rest\versions\v1\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $project_id
 * @property string $title
 * @property integer $status_id
 * @property integer $type_id
 * @property string $description
 * @property integer $rank
 *
 * @property Projects $project
 * @property Status $status
 * @property Type $type
 */
class Tasks extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'title', 'status_id', 'type_id'], 'required'],
            [['id', 'status_id', 'type_id', 'rank'], 'integer'],
            ['id', 'default', 'value' => function() {
              $res = Tasks::find()->select('max(id) as id')->where(['project_id'=>$this->project_id])->one();
              $id = $res->id + 1;
              return $id;
            }],
            [['description'], 'string'],
            [['project_id'], 'string', 'max' => 3],
            [['title'], 'string', 'max' => 255]
        ];
    }
    
    public function extraFields() {
      return ['type', 'status'];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Ключ проекта',
            'title' => 'Название',
            'status_id' => 'Статус задачи',
            'type_id' => 'Тип задачи',
            'description' => 'Описание',
            'rank' => 'Ранг',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
}
