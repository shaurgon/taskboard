<?php

namespace rest\versions\v1\controllers;

use yii\rest\ActiveController;

class StatusController extends ActiveController {
  public $modelClass = 'rest\versions\v1\models\Status';
}
