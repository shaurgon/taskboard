<?php

namespace rest\versions\v1\controllers;

use yii\rest\ActiveController;

class TaskController extends ActiveController {

  public $modelClass = 'rest\versions\v1\models\Tasks';

  public function actions() {
    $actions = [
        'project' => [
            'class' => 'rest\versions\v1\modules\ProjectAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ],
    ];
    
    return array_merge(parent::actions(), $actions);
  }
}
