<?php

namespace rest\versions\v1\controllers;

use yii\rest\ActiveController;

class TypeController extends ActiveController {
  public $modelClass = 'rest\versions\v1\models\Type';
}
