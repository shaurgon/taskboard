@echo off

set API_PATH=%~dp0%api
set ANGULAR_PATH=%~dp0%main

echo ========================================
echo = COMPOSER INSTALL                     =
echo ========================================

cd %API_PATH%
call composer install

echo ========================================
echo = NODE INSTALL                         =
echo ========================================

cd %ANGULAR_PATH%
call npm install

echo ========================================
echo = BUILD PROJECT                        =
echo ========================================

cd %ANGULAR_PATH%
call gulp