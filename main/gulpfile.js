'use strict';

var gulp = require('gulp'),
        // CSS/SCSS/LESS
        prefixer = require('gulp-autoprefixer'),
        concatCss = require('gulp-concat-css'),
        cssmin = require('gulp-minify-css'),
        sass = require('gulp-sass'),
        less = require('gulp-less'),
        jsonminify = require('gulp-jsonminify'),
        // Angular
        ngAnnotate = require('gulp-ng-annotate'),
        // JS
        uglify = require('gulp-uglify'),
        concat = require('gulp-concat'),
        sourcemaps = require('gulp-sourcemaps'),
        // System
        rimraf = require('rimraf'),
        filter = require('gulp-filter'),
        mergeStream = require('merge-stream'),
        browserSync = require('browser-sync').create(),
        spa = require('browser-sync-spa'),
        reload = browserSync.reload,
        // Bower
        bower = require('gulp-bower'),
        mainBowerFiles = require('main-bower-files');

// webserver config
var config = {
  server: {
    baseDir: "./build/"
  },
  tunnel: false,
  host: 'taskboard.dev',
  port: 9000,
  logPrefix: "frontend_daemon"
};

// Pathes
var path = {
  source: {
    html: ['./src/index.html', './src/**/*.html'],
    json: ['./src/app/json/*.json'],
    js: ['./src/app/**/module.js', './src/app/**/*.js'],
    css: ['./src/css/*.css'],
    img: './src/img/**',
    fonts: {
      fa: './bower_components/font-awesome/fonts/**.*',
      glyph: './bower_components/bootstrap/fonts/**.*'
    }
  },
  build: {
    html: config.server.baseDir,
    json: config.server.baseDir + 'app/json/',
    js: config.server.baseDir + 'app/',
    css: config.server.baseDir + 'css/',
    img: config.server.baseDir + 'img/',
    fonts: {
      fa: config.server.baseDir + 'fonts/',
      glyph: config.server.baseDir + 'bootstrap/fonts/'
    }
  }
}

// Default task
gulp.task('default', ['build'], function () {
  gulp.start('webserver');
  gulp.start('watch');
});

// Main build task
gulp.task('build', ['clean', 'bower-css', 'bower-js', 'bower-fonts'], function () {
  gulp.start('html');
  gulp.start('img');
  gulp.start('js');
});

// Start Webserver
gulp.task('webserver', ['html', 'json', 'img', 'js', 'css'], function () {
  browserSync.use(spa({
    selector: "[ng-app]"
  }));
  browserSync.init(config);
});

// Changes monitoring
gulp.task('watch', function () {
  gulp.watch(path.source.html, ['html']);
  gulp.watch(path.source.json, ['json']);
  gulp.watch(path.source.js, ['js']);
  gulp.watch(path.source.css, ['css']);
});

// Build HTML. Only copy/paste
gulp.task('html', function () {
  return gulp.src(path.source.html)
          .pipe(gulp.dest(path.build.html))
          .pipe(reload({stream: true}));
});

// Build JSON
gulp.task('json', function () {
  return gulp.src(path.source.json)
          .pipe(jsonminify())
          .pipe(gulp.dest(path.build.json))
          .pipe(reload({stream: true}));
});

// Build IMG. Only copy/paste
gulp.task('img', function () {
  return gulp.src(path.source.img)
          .pipe(gulp.dest(path.build.img))
          .pipe(reload({stream: true}));
});

// Build JS/Angular
gulp.task('js', function () {
  return gulp.src(path.source.js)
//          .pipe(sourcemaps.init())
          .pipe(concat('app.min.js'))
          .pipe(ngAnnotate())
          .pipe(uglify())
//          .pipe(sourcemaps.write())
          .pipe(gulp.dest(path.build.js))
          .pipe(reload({stream: true}));
});

// Build CSS
gulp.task('css', function () {
  return gulp.src(path.source.css)
//          .pipe(sourcemaps.init())
          .pipe(concatCss('style.min.css'))
          .pipe(prefixer())
          .pipe(cssmin())
//          .pipe(sourcemaps.write())
          .pipe(gulp.dest(path.build.css))
          .pipe(reload({stream: true}));
})

// Build Bower files
gulp.task('bower', ['bower-update'])

// Bower update
gulp.task('bower-update', function () {
  return bower();
})

// Compile Bower JS files
gulp.task("bower-js", ['bower-update'], function () {
  return gulp.src(mainBowerFiles())
          .pipe(filter('**.js'))
//          .pipe(sourcemaps.init())
          .pipe(concat('vendor.min.js'))
          .pipe(uglify())
//          .pipe(sourcemaps.write())
          .pipe(gulp.dest(path.build.js))
});

// Compile Bower CSS/LESS/SCSS files
gulp.task("bower-css", ['bower-update'], function () {
  var sassStream, lessStream, cssStream;
  lessStream = gulp.src(mainBowerFiles())
          .pipe(filter('**.less'))
          .pipe(less());

  sassStream = gulp.src(mainBowerFiles())
          .pipe(filter(['**.scss', '**.sass']))
          .pipe(sass());

  cssStream = gulp.src(mainBowerFiles())
          .pipe(filter('**.css'));

  return mergeStream(lessStream, sassStream)
          .add(cssStream)
//          .pipe(sourcemaps.init())
          .pipe(concatCss('vendor.min.css'))
          .pipe(prefixer())
          .pipe(cssmin())
//          .pipe(sourcemaps.write())
          .pipe(gulp.dest(path.build.css))
});

gulp.task("bower-fonts", ['bower-update'], function () {
  gulp.src(path.source.fonts.fa)
          .pipe(gulp.dest(path.build.fonts.fa))
  gulp.src(path.source.fonts.glyph)
          .pipe(gulp.dest(path.build.fonts.glyph))
})

// Clear build directory
gulp.task('clean', function (cb) {
  rimraf(config.server.baseDir + '/*', cb);
});

