;
(function () {
  "use strict";
  angular
      .module("tb.projects", [])
      .config(function ($stateProvider) {
        $stateProvider
            .state('tb.projects', {
              url: 'projects',
              views: {
                'content': {
                  templateUrl: '/app/projects/projects.content.html',
                  controller: 'projectsCtrl'
                }
              }
            });
      })
      .controller('projectsCtrl', projectsCtrl)
      .controller('createCtrl', createCtrl)
      .factory('Projects', function (Restangular) {
        return Restangular.all('projects');
      });

  function projectsCtrl($scope, $state, $uibModal, Projects) {
    Projects.getList().then(function (res) {
      $scope.projects = res;
    })

    $scope.createProject = function () {
      var options = {
        animation: true,
        templateUrl: '/app/projects/projects.create.html',
        controller: 'createCtrl'
      };
      $scope.editor(options);
    }

    // Modal
    $scope.editor = function (options) {
      var modalInstance = $uibModal.open(options);
      modalInstance.result.then(function (item) {
        $state.go('tb.projects', {}, {reload: true});
      });
    };
  }

  function createCtrl($scope, $uibModalInstance, Projects, toaster) {
    $scope.project = {
      name: '',
      id: '',
    }

    // Send data
    $scope.ok = function () {
      var check = /^\w{3}$/i;
      if(!check.test($scope.project.id)) {
        toaster.pop('warning', 'Внимание', 'Ключ должен состоять из 3 английских символов или цифр')
      } else {
        Projects.post($scope.project).then(function () {
          toaster.pop('success', 'Отлично', 'Проект создан')
          $uibModalInstance.close($scope.project);
        });
      }
    };

    // Cancel
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})();