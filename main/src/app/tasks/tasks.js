;
(function () {
  "use strict";
  angular
      .module("tb.tasks", [])
      .config(function ($stateProvider) {
        $stateProvider
            .state('tb.tasks', {
              url: ':project/tasks',
              views: {
                'content': {
                  templateUrl: '/app/tasks/tasks.content.html',
                  controller: 'tasksCtrl'
                }
              },
            })
            .state('tb.alltasks', {
              url: 'tasks',
              views: {
                'content': {
                  templateUrl: '/app/tasks/tasks.content.html',
                  controller: 'tasksCtrl'
                }
              }
            });
      })
      .controller('tasksCtrl', tasksCtrl)
      .controller('createTaskCtrl', createTaskCtrl)
      .factory('Tasks', function (Restangular) {
        return Restangular.all('tasks');
      })
      .factory('Statuses', function (Restangular) {
        return Restangular.all('statuses');
      })
      .factory('Types', function (Restangular) {
        return Restangular.all('types');
      });

  function tasksCtrl($rootScope, $scope, $state, $stateParams, $uibModal, Projects, Tasks, Statuses, Types) {
    var project = ($stateParams.project) ? $stateParams.project : '';
    Projects.get(project).then(function(res){
      $scope.projectName = res.name;
    })
    
    Tasks.get(project, {expand: 'status,type'}).then(function (res) {
      $scope.tasks = res;
    });

    if (!$rootScope.$storage.statuses) {
      Statuses.getList().then(function (res) {
        $rootScope.$storage.statuses = res;
      });
    }

    if (!$rootScope.$storage.types) {
      Types.getList().then(function (res) {
        $rootScope.$storage.types = res;
      });
    }
    ;

    $scope.createTask = function () {
      var options = {
        animation: true,
        templateUrl: '/app/tasks/tasks.create.html',
        controller: 'createTaskCtrl',
        resolve: {
          data: function () {
            return {
              title: '',
              id: '',
              project_id: project,
              type_id: 1,
              status_id: 1,
              rank: 0
            }
          }
        }
      };
      $scope.editor(options);
    }

    $scope.editTask = function (task) {
      var options = {
        animation: true,
        templateUrl: '/app/tasks/tasks.create.html',
        controller: 'createTaskCtrl',
        resolve: {
          data: function () {
            return task;
          }
        }
      };
      $scope.editor(options);
    }

    // Modal
    $scope.editor = function (options) {
      var modalInstance = $uibModal.open(options);
      modalInstance.result.then(function (item) {
        $state.go("tb.tasks", {project: project}, {reload: true});
      });
    };
  }

  function createTaskCtrl($scope, $uibModalInstance, data, toaster, Tasks, Restangular) {
    var newTask = (data.id == '') ? true : false;
    $scope.task = data;
    console.info(data)
    $scope.label = (newTask) ? 'Создание' : 'Редактирование';

    // Send data
    $scope.ok = function () {
      if (newTask) {
        Tasks.post($scope.task).then(function () {
          toaster.pop('success', 'Отлично', 'Задача создана');
          $uibModalInstance.close($scope.task);
        });
      } else {
        var id = data.id + ',' + data.project_id;
        Tasks.customPUT(data, id).then(function (res) {
          toaster.pop('success', 'Отлично', 'Задача обновлена');
          $uibModalInstance.close($scope.task);
        })
      }
    };

    // Cancel
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})();