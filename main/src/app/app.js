;
(function () {
  "use strict";
  angular
      .module("tb", ['ui.router', 'restangular', 'ngStorage', 'toaster', 'ui.bootstrap', 'tb.projects', 'tb.tasks'])
      .run(function ($rootScope, $state, $stateParams, $sessionStorage, toaster, Restangular) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$storage = $sessionStorage.$default({
          token: '',
          statuses: false,
          types: false
        });

        $rootScope.logout = function () {
          $rootScope.$storage.token = '';
        };

        Restangular.addErrorInterceptor(function (response, deferred, responseHandler) {
          if (response.status === 422) {
            _.forEach(response.data, function (res) {
              toaster.pop("warning", "Проверка данных", res.message)
            })
            return false;
          }

          return true; // error not handled
        })
      })
      .config(function ($locationProvider, $stateProvider, RestangularProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
        // Конфиг для IIS
        RestangularProvider.setBaseUrl('http://localhost/taskboard/api/rest/web/v1/');
        // Если не лениво настроить виртуалхост
        //RestangularProvider.setBaseUrl('http://apitask.elhazard.in/v1');
        RestangularProvider.setMethodOverriders(["put", "delete"]);

        $stateProvider
            .state('tb', {
              url: '/',
              views: {
                '@': {
                  templateUrl: '/app/templates/index.html'
                },
                'content@tb': {
                  templateUrl: '/app/projects/projects.content.html',
                  controller: 'projectsCtrl'
                }
              }
            });
      });
})();