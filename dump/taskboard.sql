﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.358.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 26.10.2015 14:02:59
-- Версия сервера: 5.5.42
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE taskboard;

--
-- Описание для таблицы projects
--
DROP TABLE IF EXISTS projects;
CREATE TABLE projects (
  id VARCHAR(3) NOT NULL,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы status
--
DROP TABLE IF EXISTS status;
CREATE TABLE status (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы type
--
DROP TABLE IF EXISTS type;
CREATE TABLE type (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы tasks
--
DROP TABLE IF EXISTS tasks;
CREATE TABLE tasks (
  id INT(11) NOT NULL,
  project_id VARCHAR(3) NOT NULL DEFAULT '',
  title VARCHAR(255) NOT NULL,
  status_id INT(11) NOT NULL,
  type_id INT(11) NOT NULL,
  description TEXT DEFAULT NULL,
  rank INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (id, project_id),
  CONSTRAINT FK_tasks_projects_id FOREIGN KEY (project_id)
    REFERENCES projects(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT FK_tasks_status_id FOREIGN KEY (status_id)
    REFERENCES status(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_tasks_type_id FOREIGN KEY (type_id)
    REFERENCES type(id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы projects
--
INSERT INTO projects VALUES
('trg', 'Второй проект');

-- 
-- Вывод данных для таблицы status
--
INSERT INTO status VALUES
(1, 'Новая'),
(2, 'В работе'),
(3, 'Выполнена');

-- 
-- Вывод данных для таблицы type
--
INSERT INTO type VALUES
(1, 'Задача'),
(2, 'Улучшение'),
(3, 'Баг');

-- 
-- Вывод данных для таблицы tasks
--
INSERT INTO tasks VALUES
(1, 'trg', 'Тестовая задача', 1, 1, 'Длинное описание. Ну очень-очень длинное =)', 0);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;